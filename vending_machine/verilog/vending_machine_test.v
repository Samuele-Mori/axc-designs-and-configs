`timescale 1 ns / 1 ns

`define WARNING
`define INFO
`define TEST_INFO

module vending_machine_test();
 reg c1, c2, in0, in1, in2, in3, cnl, clk, rst;
	reg item0_available, item1_available, item2_available, item3_available;
	wire pdt;
	wire [2:0] cng;
	wire [2:0] rtn;

	integer out_log_file;
	integer tick = 0;
	localparam delta_t = 200;

	vending_machine dut(.c1(c1), .c2(c2), .in0(in0), .in1(in1), .in2(in2), .in3(in3),
			    .cnl(cnl), .clk(clk), .rst(rst), .item0_available(item0_available),
			    .item1_available(item1_available), .item2_available(item2_available),
			    .item3_available(item3_available),
			    .pdt(pdt), .cng(cng), .rtn(rtn));
	
	initial begin
	out_log_file = $fopen("./timing_diagrams/out_log.txt", "w");
		
		/* Suppose pull-down everything init with zero values */
		cnl = 0;
		/* Indicate that all items are available */
		item0_available = 1'b1;
		item1_available = 1'b1;
		item2_available = 1'b1;
		item3_available = 1'b1;

		in0 = 1'b0;
		in1 = 1'b0;
		in2 = 1'b0;
		in3 = 1'b0;

		c1 = 1'b0;
		c2 = 1'b0;

		/* Reset active low */
		rst = 1'b1;
		/* Clock starting with high */
		clk = 1'b1;
		#5;
		/* Init the vending machine */
		rst = 1'b0;
		#5;

		/* Select item0 - VAFLA_BOROVEC */
		in0 = 1'b1;
		#10;
		in0 = 1'b0;
		$display("[USER INPUT] First item has been selected!");

`ifdef TEST_SCENARIO
		#10
		/* Insert one coin with value 2 */
		c1  = 1'b1;
		#2;
		c1  = 1'b0;
		$display("[USER INPUT] Coin 1 with value 1 inserted!");
`else
		#10
		/* Insert one coin with value 2 */
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");
`endif

		/* Insert one coin with value 2 */
		#2
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#9
		in1 = 1'b1;
		#10;
		in1 = 1'b0;

		/* Insert one coin with value 2 */
		#2
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#2
		/* Insert one coin with value 2 */
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#9
		in2 = 1'b1;
		#10;
		in2 = 1'b0;

		/* Insert one coin with value 2 */
		#2
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#2
		/* Insert one coin with value 2 */
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#2
		/* Insert one coin with value 2 */
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#9
		in3 = 1'b1;
		#10;
		in3 = 1'b0;

		/* Insert one coin with value 2 */
		#2
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#2
		/* Insert one coin with value 2 */
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		/* Insert one coin with value 2 */
		#2
		c1  = 1'b1;
		#2;
		c1  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		/* Insert one coin with value 2 */
		#2
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#9
		in3 = 1'b1;
		#10;
		in3 = 1'b0;

		/* Insert one coin with value 2 */
		#2
		c2  = 1'b1;
		#2;
		c2  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#2
		/* Insert one coin with value 2 */
		c1  = 1'b1;
		#2;
		c1  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#2
		/* Insert one coin with value 2 */
		c1  = 1'b1;
		#2;
		c1  = 1'b0;
		$display("[USER INPUT] Coin 2 with value 2 inserted!");

		#2
		c1  = 1'b1;
		#2;
		c1  = 1'b0;

		#3
		cnl = 1'b1;
		#2
		cnl = 1'b0;
	end
	
	always #1
       	begin 
		clk   = ~clk;
		tick = tick + 1;
		
		$fwrite(out_log_file,"CLK = %b.\n", clk);
		$fwrite(out_log_file,"RESET = %b.\n", rst);
		$fwrite(out_log_file,"IN0 = %b.\n", in0);
		$fwrite(out_log_file,"IN1 = %b.\n", in1);
		$fwrite(out_log_file,"IN2 = %b.\n", in2);
		$fwrite(out_log_file,"IN3 = %b.\n", in3);
		$fwrite(out_log_file,"COIN1 = %b.\n", c1);
		$fwrite(out_log_file,"COIN2 = %b.\n", c2);
		$fwrite(out_log_file,"CNL = %b.\n", cnl);
		$fwrite(out_log_file,"ITEM0_AVAILABLE = %b.\n", item0_available);
		$fwrite(out_log_file,"ITEM1_AVAILABLE = %b.\n", item1_available);
		$fwrite(out_log_file,"ITEM2_AVAILABLE = %b.\n", item2_available);
		$fwrite(out_log_file,"ITEM3_AVAILABLE = %b.\n", item3_available);
		$fwrite(out_log_file,"PDT = %b.\n", pdt);
		$fwrite(out_log_file,"CHANGE_0 = %b.\n", cng[0]);
		$fwrite(out_log_file,"CHANGE_1 = %b.\n", cng[1]);
		$fwrite(out_log_file,"CHANGE_2 = %b.\n", cng[2]);
		$fwrite(out_log_file,"RETURN_0 = %b.\n", rtn[0]);
		$fwrite(out_log_file,"RETURN_1 = %b.\n", rtn[1]);
		$fwrite(out_log_file,"RETURN_2 = %b.\n", rtn[2]);
	
		if (delta_t == tick)
		begin
			$finish;
		end
	end	


`ifdef TEST_INFO
	always #2
	begin
		if (pdt)
			$display("[MACHINE OUTPUT] PDT = 1 CNG = %d", cng);
		if (rtn)
			$display("[MACHINE OUTPUT] Coins returned after cancleing order = %d", rtn);
	end
`endif

endmodule
