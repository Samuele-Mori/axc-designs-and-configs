module masterFSM(clk, rst, start, doneGCD, startGCD, done);
    input clk, rst, start, doneGCD;
    output startGCD, done;

    reg state, ns;
    reg startGCD, done;
    parameter idle = 1'b0;
    parameter gcd  = 1'b1;

    always @(posedge clk or posedge rst) begin
        if (rst) state <= idle;
        else     state <= ns;
    end

    always @(*) begin
        case (state)
            idle: begin
                startGCD = 1'b0;
                done = 1'b0;
                ns = idle;
                if (start) begin
                    startGCD = 1'b1;
                    ns = gcd;
                end
            end
            default : begin
                startGCD = 1'b0;
                done = 1'b0;
                ns = gcd;
                if (doneGCD) begin
                    ns = idle;
                    done = 1'b1;
                end
            end
        endcase
    end
endmodule
