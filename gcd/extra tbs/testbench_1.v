

// AB: testbench with big numbers

module testbench();
    parameter cyc = 10; // clock cycle
    parameter delay = cyc/2;
    parameter delta = cyc/5;

    wire done, error;
    wire [31:0] result;
    reg clk, rst, start;
    reg [31:0] aa, bb;

    GCD gcd0(clk, rst, start, aa, bb, done, result, error);

    // clock
    always #(cyc/2) clk = ~clk;

    initial begin
        rst = 1'b0;
        clk = 1'b1;
        start = 1'b0;
        #(cyc);
        #(delay) rst = 1;
        #(cyc*4) rst = 0;
        #(delay);
		
        
        // --------- TEST_1
        $display(" \n\n-------- TEST 1 -------- ");
        #(delta);
        aa=32'd1000; // 2*2*2*5*5*5
        bb=32'd1345; // 5*269
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

 		

    
        // --------- TEST_11
        $display(" \n\n-------- TEST 11 -------- ");
        #(delta);
        aa=32'd256945; // 5*13*59*67 
        bb=32'd329485; // 5*13*37*137
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

        // --------- TEST_12
        $display(" \n\n-------- TEST 12 -------- ");
        #(delta);
        aa=32'd4000000000; // 2^11*5^9
        bb=32'd278943;     // 3*7*37*359
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

        // --------- TEST_13
        $display(" \n\n-------- TEST 13 -------- ");
        #(delta);
        aa=32'd4294000000; // 2^7*5^6*19*113
        bb=32'd4294960000; // 2^7*5^4*37*1451
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

        // --------- TEST_14
        $display(" \n\n-------- TEST 14 -------- ");
        #(delta);
        aa=32'd3120120120; // 2^3*3*5*13*541*3697 
        bb=32'd2727272727; // 3^3*41*271*9091
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

        // --------- TEST_15
        $display(" \n\n-------- TEST 15 -------- ");
        #(delta);
        aa=32'd4000003243; // 4000003243 
        bb=32'd4000000006; // 2*17*211*233*2393
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

		
		$display(" \n\n -------- TEST ENDED --------");

        #(2*cyc);
        $finish;
    end
endmodule
