

// AB: testbench with small numbers

module testbench();
    parameter cyc = 10; // clock cycle
    parameter delay = cyc/2;
    parameter delta = cyc/5;

    wire done, error;
    wire [31:0] result;
    reg clk, rst, start;
    reg [31:0] aa, bb;

    GCD gcd0(clk, rst, start, aa, bb, done, result, error);

    // clock
    always #(cyc/2) clk = ~clk;

    initial begin
        rst = 1'b0;
        clk = 1'b1;
        start = 1'b0;
        #(cyc);
        #(delay) rst = 1;
        #(cyc*4) rst = 0;
        #(delay);
		
        
   
        // --------- TEST_2
        $display(" \n\n-------- TEST 2 -------- ");
        #(delta);
        aa=32'd1; // 1 
        bb=32'd1; // 1
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);	
		
		// --------- TEST_3
		$display(" \n\n-------- TEST 3 -------- ");
        #(delta);
        aa=32'd144; // 2*2*2*2*3*3
        bb=32'd180; // 2*2*3*3*5
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

        #(cyc);
        #(delay) rst = 1;
        #(cyc*4) rst = 0;
        #(delay);

        // --------- TEST_4
        $display(" \n\n-------- TEST 4 -------- ");
        #(delta);
        aa=32'd76; // 2*2*19
        bb=32'd51; // 3*17
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

        #(cyc);
        #(delay) rst = 1;
        #(cyc*4) rst = 0;
        #(delay);

        // --------- TEST_5
        $display(" \n\n-------- TEST 5 -------- ");
        #(delta);
        aa=32'd660; // 3*4*5*11
        bb=32'd252; // 2*2*3*3*7
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

        #(cyc);
        #(delay) rst = 1;
        #(cyc*4) rst = 0;
        #(delay);

        // --------- TEST_6
        $display(" \n\n-------- TEST 6 -------- ");
        #(delta);
        aa=32'd997; // 997
        bb=32'd991; // 991
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

        // --------- TEST_7
        $display(" \n\n-------- TEST 7 -------- ");
        #(delta);
        aa=32'd0;  // 0 --> ERRORR
        bb=32'd27; // 3*3*3
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

        // --------- TEST_8
        $display(" \n\n-------- TEST 8 -------- ");
        #(delta);
        aa=32'd1011; // 3*337
        bb=32'd1011; // 3*337
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

        // --------- TEST_10
        $display(" \n\n-------- TEST 10 -------- ");
        #(delta);
        aa=32'd7; // 7 
        bb=32'd0; // 0 --> ERROR
        start = 1'b1;
        $display("aa = %d, bb = %d", aa, bb);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, error = %d", result, error);
        #(delay);
        #(cyc);

  
		
		$display(" \n\n -------- TEST ENDED --------");

        #(2*cyc);
        $finish;
    end
endmodule
