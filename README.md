# Modelsim 

### Vcd generation
On cmd:
```sh
vsim -c -voptargs="+acc" -do "vcd file work/CHOOSENAME.vcd; vcd add /*; run -all; quit" work.TBMODULENAME
```
E.g.:
```sh
vsim -c -voptargs="+acc" -do "vcd file work/sobel.vcd; vcd add /*; run -all; quit" work.sobel_tb
```

### Coverage file generation
Once Modelsim is started:
```sh
vlog *.v +cover=bs -coveropt 1
vsim -c -coverage topModuleName //top=tbModuleName in many cases
run -all
coverage save PATH/specific_coverage.ucdb
vcover report specific_coverage.ucdb -details -output specific_coverage.txt
```

# Harm
```sh
./harm --vcd path/VCDFILE.vcd --clk CLOCK -c path/HARMCONFIG.xml --dump
```

